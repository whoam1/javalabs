package com.company;

import java.util.UUID;

public abstract class Device implements ICrudAction {
    protected UUID ID;
    protected String companyName;
    protected String model;
    protected String os;
    protected int memory;
    protected int cost;
    protected static int counter;

    Device(){
        ID = UUID.randomUUID();
        counter++;
    }

    Device(String companyName, String model, String os, int memory, int cost){
        ID = UUID.randomUUID();
        this.companyName = companyName;
        this.memory = memory;
        this.model = model;
        this.os = os;
        this.memory = memory;
        this.cost = cost;
        counter++;
    }
}
