package com.company;

import java.util.Scanner;
//
public class Main {

    public static void main(String[] args) {

        Phone[] phonesArray;
        Smartphone[] smartPhonesArray;
        Tablet[] tabletsArray;

        int N = Integer.parseInt(args[0]);
        String type = args[1];
        System.out.println("Please, enter your devices: ");

        switch (type.charAt(0)) {
            case 'p': {
                phonesArray = new Phone[N];
                for (int i = 0; i < N; i++) {
                    phonesArray[i] = new Phone();
                    phonesArray[i].update();
                }
                System.out.println("Data is in list. Show? [y/n]");
                Scanner in = new Scanner(System.in);
                String answer = in.nextLine();
                if (answer.equals("y"))
                    for (int i = 0; i < N; i++)
                        phonesArray[i].read();
                else if (answer.equals("n")) break;
                else System.out.println("Incorrect command");
            }
            break;
            case 's': {
                smartPhonesArray = new Smartphone[N];
                for (int i = 0; i < N; i++) {
                    smartPhonesArray[i] = new Smartphone();
                    smartPhonesArray[i].update();
                }
                System.out.println("Data is in list. Show? [y/n]");
                Scanner in = new Scanner(System.in);
                String answer = in.nextLine();
                if (answer.equals("y"))
                    for (int i = 0; i < N; i++)
                        smartPhonesArray[i].read();
                else if (answer.equals("n")) break;
                else System.out.println("Incorrect command");
            }
            break;
            case 't': {
                tabletsArray = new Tablet[N];
                for (int i = 0; i < N; i++) {
                    tabletsArray[i] = new Tablet();
                    tabletsArray[i].update();
                }
                System.out.println("Data is in list. Show? [y/n]");
                Scanner in = new Scanner(System.in);
                String answer = in.nextLine();
                if (answer.equals("y"))
                    for (int i = 0; i < N; i++)
                        tabletsArray[i].read();
                else if (answer.equals("n")) break;
                else System.out.println("Incorrect command");
            }
            break;
            default:
                System.out.println("Incorrect data");
                break;
        }
    }
}
