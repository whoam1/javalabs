package com.company;

import java.util.Scanner;
import java.util.UUID;

public class Smartphone extends Device {

    private String typeOfSim;
    private int numberOfSim;

    Smartphone() {
        super();
    }

    Smartphone(String companyName, String model, String os, String typeOfSim, int numberOfSim, int memory, int cost) {
        super(companyName, model, os, memory, cost);
        this.typeOfSim = typeOfSim;
        this.numberOfSim = numberOfSim;
    }

    @Override
    public void create() {
        String[] companyNames = {"Xiaomi", "Sony", "Samsung", "HTC", "Huawei"};
        String[] modelNames = {"View 10", "P20 Pro", "U11 EYEs", "XZ2", "Nova"};
        String[] osNames = {"Android 4.3", "Android 4.4", "Android 5.0", "Android 5.1", "Android 6.0"};
        int[] sizesOfMem = {64, 128, 32, 256, 16};
        String[] typesOfSim = {"Micro-SIM", "Classic"};

        int numberOfCompanyName = (int) (Math.random() * 4);
        int numberOfModelName = (int) (Math.random() * 4);
        int numberOfOsName = (int) (Math.random() * 4);
        int numberOfMem = (int) (Math.random() * 4);
        int numberOfType = (int) (Math.random() * 2);

        companyName = companyNames[numberOfCompanyName];
        model = modelNames[numberOfModelName];
        os = osNames[numberOfOsName];
        memory = sizesOfMem[numberOfMem];
        typeOfSim = typesOfSim[numberOfType];
        numberOfSim = (int) (Math.random() * 2);
        cost = 250 + (int) (Math.random() * 1000);
    }

    @Override
    public void read() {
        System.out.println("\nID: " + ID + "\n№: " + counter + "\nCompany: " + companyName + "\nModel: " + model + "\nOS: " + os + "\nSIM: " + typeOfSim + "\nNumber of SIM: " + numberOfSim + "\nMemory: " + memory + "GB\nCost: " + cost + "$");
    }

    @Override
    public void update() {
        Scanner in = new Scanner(System.in);
        System.out.println("\nCompany: ");
        companyName = in.nextLine();
        System.out.println("\nModel: ");
        model = in.nextLine();
        System.out.println("\nOS: ");
        os = in.nextLine();
        System.out.println("\nSIM: ");
        typeOfSim = in.nextLine();
        System.out.println("\nNumber of SIM: ");
        numberOfSim = in.nextInt();
        System.out.println("\nMemory (GB): ");
        memory = in.nextInt();
        System.out.println("\nCost: ");
        cost = in.nextInt();
    }

    @Override
    public void delete() {
        ID = null;
        companyName = null;
        model = null;
        os = null;
        memory = 0;
        typeOfSim = null;
        numberOfSim = 0;
        cost = 0;
        counter--;
    }
}
