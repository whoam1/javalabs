package com.company;

import java.util.ArrayList;
import java.util.TreeMap;

public class Orders<T extends Order> {
    private ArrayList<T> orders;
    private TreeMap treeMap;

    Orders() {
        orders = new ArrayList<>();
        treeMap = new TreeMap<>();
    }

    void issueOrders(ShoppingCart cart, Credentials user) {
        T order = (T)new Order(cart, user);
        treeMap.put(order.getTimeOfCreation(), order);
        orders.add(order);
    }

    void checkOrders() {
        for (int i = 0; i < orders.size(); i++)
            if (orders.get(i).checkStatus())
                orders.remove(i);
    }

    void showAll() {
        checkOrders();
        System.out.println("\nYour orders: \n");
        int i = 0;
        System.out.println("+++++++++++++++++++++++++++++++++++++");
        while (i < orders.size()) {
            System.out.println("\n-------------- ORDER #" + (i + 1) + " --------------");
            orders.get(i).getCart().showAll();
            i++;
        }
        System.out.println("\n+++++++++++++++++++++++++++++++++++++\n");
    }
}

