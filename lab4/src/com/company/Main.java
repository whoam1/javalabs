package com.company;

public class Main {

    public static void main(String[] args) {
        Orders<Order> orders = new Orders<>();
        ACheck checkTread;
        ACheck removeItemsThread;
        OrderCreator orderCreator;
        for (int i = 0; i < 3; i++) {
            orderCreator = new OrderCreator("Поток генерации", orders);
            checkTread = new CheckThread("Поток проверки на состояние", orders);
            removeItemsThread = new RemoveItemsThread("Поток удаления обработанных заказов", orders);
            try {
                orderCreator.getT().join();
                checkTread.getT().join();
                removeItemsThread.getT().join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            orders.showAll();
        }
    }
}
