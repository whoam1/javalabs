package com.company;


public class RemoveItemsThread extends ACheck {


    RemoveItemsThread(Orders<Order> orders){
        super(orders);
    }

    RemoveItemsThread(String name, Orders<Order> orders){
        super(name, orders);
    }

    @Override
    public void run() {
        System.out.println("\n" + name + " в работе: " + getT().isAlive() + "\n");
        for (int i = orders.getOrders().size() - 1; i >= 0;  --i)
          if (orders.getOrders().get(i).isStatus())
                orders.getOrders().remove(i);

    }
}

