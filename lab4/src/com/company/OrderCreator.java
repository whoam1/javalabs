package com.company;

public class OrderCreator extends ACheck {
    private ShoppingCart<Device> cart;
    private Credentials user;

    OrderCreator(Orders<Order> orders) {
        super(orders);
        user = new Credentials("Vladislav", "Evg", "Bankov", "bvl.24@icloud.com");
    }

    OrderCreator(String name, Orders<Order> orders) {
        super(name, orders);
        user = new Credentials("Vladislav", "Evg", "Bankov", "bvl.24@icloud.com");
    }

    private Order randomShoppingCart() {
        cart = new ShoppingCart<>();
        int randItem;
        int randI = (int) (Math.random() * 4) + 1;
        Device a = new Phone();
        Device b = new Smartphone();
        Device c = new Tablet();
        a.create();
        b.create();
        c.create();
        Device[] tmp = {a, b, c};
        for (; randI >= 0; --randI) {
            randItem = (int) (Math.random() * 3);
            cart.add(tmp[randItem]);
        }
        Order order = new Order(cart, user);
        return order;
    }

    @Override
    public void run() {
        System.out.println("\n" + name + " в работе: " + getT().isAlive() + "\n");
        for (int i = 0; i < 3; i++)
            orders.getOrders().add(randomShoppingCart());
    }
}
