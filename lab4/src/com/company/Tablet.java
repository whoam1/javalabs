package com.company;

import java.util.Scanner;
import java.util.UUID;

public class Tablet extends Device {

    private String gpu;
    private String screenResolution;

    Tablet() {
        super();
    }

    Tablet(String companyName, String model, String os, String gpu, String screenResolution, int numberOfSim, int memory, int cost) {
        super(companyName, model, os, memory, cost);
        this.gpu = gpu;
        this.screenResolution = screenResolution;
    }

    @Override
    public void create() {
        String[] companyNames = {"Microsoft", "ASUS", "Samsung", "Google", "Huawei"};
        String[] modelNames = {"TAB S", "Surface PRO", "ZENPAD 3S 10", "XZ2", "Pixel C"};
        String[] osNames = {"Android 4.3", "Android 4.4", "Android 5.0", "Android 5.1", "Android 6.0"};
        int[] sizesOfMem = {64, 128, 32, 256, 16};
        String[] namesOfGpu = {"Qualcomm Adreno 430", "NVIDIA GeForce Tegra K1", "PowerVR GX6450", "ARM Mali-T760", "PowerVR GSX 544 MP4"};
        String[] screenResolutions = {"2048 х 1536", "2224 х 1668", "2736 х 1824", "2560 х 1800", "2732 х 2048"};

        int numberOfCompanyName = (int) (Math.random() * 4);
        int numberOfModelName = (int) (Math.random() * 4);
        int numberOfOsName = (int) (Math.random() * 4);
        int numberOfMem = (int) (Math.random() * 4);
        int numberOfGpu = (int) (Math.random() * 4);
        int numberOfScreenResolution = (int) (Math.random() * 4);

        companyName = companyNames[numberOfCompanyName];
        model = modelNames[numberOfModelName];
        os = osNames[numberOfOsName];
        memory = sizesOfMem[numberOfMem];
        gpu = namesOfGpu[numberOfGpu];
        screenResolution = screenResolutions[numberOfScreenResolution];
        cost = 250 + (int) (Math.random() * 1000);
    }

    @Override
    public void read() {
        System.out.println("\nID: " + ID + "\nCompany: " + companyName + "\nModel: " + model + "\nOS: " + os + "\nGPU: " + gpu + "\nScreen Resolution: " + screenResolution + "\nMemory: " + memory + "GB\nCost: " + cost + "$");

    }

    @Override
    public void update() {
        Scanner in = new Scanner(System.in);
        System.out.println("\nCompany: ");
        companyName = in.nextLine();
        System.out.println("\nModel: ");
        model = in.nextLine();
        System.out.println("\nOS: ");
        os = in.nextLine();
        System.out.println("\nGPU: ");
        gpu = in.nextLine();
        System.out.println("\nScreen Resolution: ");
        screenResolution = in.nextLine();
        System.out.println("\nMemory (GB): ");
        memory = in.nextInt();
        System.out.println("\nCost: ");
        cost = in.nextInt();
        in.close();
    }

    @Override
    public void delete() {
        ID = null;
        companyName = null;
        model = null;
        os = null;
        memory = 0;
        gpu = null;
        screenResolution = null;
        cost = 0;
        counter--;
    }
}
