package com.company;

public class CheckThread extends ACheck {

    CheckThread(Orders<Order> orders){
        super(orders);
    }

    CheckThread(String name, Orders<Order> orders){
        super(name, orders);
    }

    @Override
    public void run() {
        System.out.println("\n" + name + " в работе: " + getT().isAlive() + "\n");
        for (int i = 0; i < orders.getOrders().size(); i++)
            orders.getOrders().get(i).checkStatus();
    }
}
