package com.company;


import java.util.Scanner;
import java.util.UUID;

public class Phone extends Device {

    private String typeOfCase;

    Phone() {
        super();
    }

    Phone(String companyName, String model, String os, String typeOfCase, int memory, int cost) {
        super(companyName, model, os, memory, cost);
        this.typeOfCase = typeOfCase;
    }

    @Override
    public void create() {
        // ID = UUID.randomUUID();
        String[] companyNames = {"Nokia", "Motorola", "LG", "Siemens", "Fly"};
        String[] modelNames = {"3310", "ME45", "T68i", "C100", "MPx200"};
        String[] osNames = {"Symbian", "Bada", "Qtopia", "MeeGo", "Sailfish"};
        int[] sizesOfMem = {64, 128, 32, 256, 512};
        String[] typesOfCase = {"Classic", "Clamshell"};

        int numberOfCompanyName = (int) (Math.random() * 4);
        int numberOfModelName = (int) (Math.random() * 4);
        int numberOfOsName = (int) (Math.random() * 4);
        int numberOfMem = (int) (Math.random() * 4);
        int numberOfType = (int) (Math.random() * 2);

        companyName = companyNames[numberOfCompanyName];
        model = modelNames[numberOfModelName];
        os = osNames[numberOfOsName];
        memory = sizesOfMem[numberOfMem];
        typeOfCase = typesOfCase[numberOfType];
        cost = 12 + (int) (Math.random() * 51);
        // counter++;
    }

    @Override
    public void read() {
        System.out.println("\nID: " + ID + "\nCompany: " + companyName + "\nModel: " + model + "\nOS: " + os + "\nCase: " + typeOfCase + "\nMemory: " + memory + "MB\nCost: " + cost + "$");
    }

    @Override
    public void update() {
        Scanner in = new Scanner(System.in);
        System.out.println("\nCompany: ");
        companyName = in.nextLine();
        System.out.println("\nModel: ");
        model = in.nextLine();
        System.out.println("\nOS: ");
        os = in.nextLine();
        System.out.println("\nCase: ");
        typeOfCase = in.nextLine();
        System.out.println("\nMemory (MB): ");
        memory = in.nextInt();
        System.out.println("\nCost: ");
        cost = in.nextInt();
        in.close();
    }

    @Override
    public void delete() {
        ID = null;
        companyName = null;
        model = null;
        os = null;
        memory = 0;
        typeOfCase = null;
        cost = 0;
        counter--;
    }
}
