package com.company;

public abstract class ACheck implements Runnable{
    protected Thread t;
    protected String name;

    protected Orders<Order> orders;

    ACheck(Orders<Order> orders){
        name = "";
        this.orders = orders;
        t = new Thread(this);
        t.start();
    }
    ACheck(String name, Orders<Order> orders){
        this.name = name;
        this.orders = orders;
        t = new Thread(this, name);
        t.start();
    }

    public Thread getT() {
        return t;
    }
}

