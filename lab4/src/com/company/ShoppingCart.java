package com.company;

import java.util.LinkedList;

public class ShoppingCart<T extends Device> {
    private LinkedList<T> cart;

    ShoppingCart() {
        cart = new LinkedList<>();
    }

    void add(T a) {
        cart.add(a);
    }

    void delete() {
        cart.removeLast();
    }

    T getItem (int index) {
        return cart.get(index);
    }
    void showAll() {
        System.out.println("\nShoppingCart elements: \n");
        int i = 0;
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        while (i < cart.size()) {
            cart.get(i).read();
            i++;
            if (i != cart.size())
                System.out.println("-------------------------------------");
        }
        System.out.println("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
    }
}
