package com.company;


public class Order {
    private boolean status;
    private long timeOfCreation;
    private long waitingTime;
    private ShoppingCart cart;
    private Credentials user;


    Order(ShoppingCart cart, Credentials user){
        status = false;
        timeOfCreation = System.currentTimeMillis();
        waitingTime = 1000 * ((int)(Math.random() * 3) + 1) + timeOfCreation;
        this.cart = cart;
        this.user = user;

    }

    public boolean isStatus() {
        return status;
    }

    public long getTimeOfCreation() {
        return timeOfCreation;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void checkStatus() {
       long currentTime = System.currentTimeMillis();
       if (currentTime >= waitingTime) status = true;
    }
}
