package com.company;

import java.util.HashSet;
import java.util.UUID;

public abstract class Device implements ICrudAction {
    protected UUID ID;
    protected String companyName;
    protected String model;
    protected String os;
    protected int memory;
    protected int cost;
    protected static int counter;
    protected static HashSet<UUID> idCollection;

    Device(){
        ID = UUID.randomUUID();
        idCollection = new HashSet<>();
        idCollection.add(ID);
        counter++;
    }

    Device(String companyName, String model, String os, int memory, int cost){
        ID = UUID.randomUUID();
        idCollection = new HashSet<>();
        idCollection.add(ID);
        this.companyName = companyName;
        this.memory = memory;
        this.model = model;
        this.os = os;
        this.memory = memory;
        this.cost = cost;
        counter++;
    }

    public boolean searchForID (UUID uuid) {
        return idCollection.contains(uuid);
    }
}
