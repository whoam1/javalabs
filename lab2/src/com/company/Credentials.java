package com.company;

import java.util.UUID;

public class Credentials {
    private UUID ID;
    private String firstName;
    private String secondName;
    private String lastName;
    private String email;

    Credentials(){
        ID = UUID.randomUUID();
    }

    Credentials(String firstName, String secondName, String lastName, String email){
        ID = UUID.randomUUID();
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.email = email;
    }
}
