package com.company;

import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        Orders orders = null;
        Credentials user = null;
        ShoppingCart cart = new ShoppingCart();
        Scanner in = new Scanner(System.in);
        int c;
        while (true) {
            System.out.println("1 - Add goods to the cart\n2 - Show my cart\n3 - Search by ID in the shopping cart\n4 - Delete the last added item\n5 - Make an order\n6 - Show my orders in processing\n7 - Complete the work with the store");
            c = in.nextInt();
            switch (c) {
                case 1: {
                    int c1;
                    boolean flag = false;
                    System.out.println("Please, choose your devices: ");
                    while (true) {
                        System.out.println("1 - Phone\n2 - Smartphone\n3 - Tablet\n4 - Go to the menu");
                        c1 = in.nextInt();
                        switch (c1) {
                            case 1: {
                                ICrudAction set = new Phone();
                                set.create();
                                cart.add(set);
                                System.out.println("Item is added in the cart");
                            }
                            break;
                            case 2: {
                                ICrudAction set = new Smartphone();
                                set.create();
                                cart.add(set);
                                System.out.println("Item is added in the cart");
                            }
                            break;
                            case 3: {
                                ICrudAction set = new Tablet();
                                set.create();
                                cart.add(set);
                                System.out.println("Item is added in the cart");
                            }
                            break;
                            case 4: {
                                flag = true;
                            }
                            break;
                            default:
                                System.out.println("Incorrect command. Please, try again...");
                                break;
                        }
                        if (flag) break;
                    }
                }
                break;
                case 2: {
                    cart.showAll();
                }
                break;
                case 3: {
                    System.out.println("Enter the identifier of the item you are looking for: ");
                    String tmp = in.nextLine();
                    String id = in.nextLine();
                    UUID uuid = UUID.fromString(id);
                    System.out.println(((Device) cart.getItem(0)).searchForID(uuid));
                }
                break;
                case 4: {
                    cart.delete();
                    System.out.println("Last added item is deleted\n");
                }
                break;
                case 5: {
                    if (orders == null) {
                        System.out.println("---Please, enter your personal data---\n");
                        String tmp = in.nextLine();
                        System.out.println("First name: ");
                        String firstName = in.nextLine();
                        System.out.println("Patronymic: ");
                        String secondName = in.nextLine();
                        System.out.println("Last name: ");
                        String lastName = in.nextLine();
                        System.out.println("Email: ");
                        String email = in.nextLine();
                        user = new Credentials(firstName, secondName, lastName, email);
                        orders = new Orders();
                        orders.issueOrders(cart, user);
                        cart = new ShoppingCart();
                    } else {
                        orders.issueOrders(cart, user);
                        cart = new ShoppingCart();
                    }
                    System.out.println("Order is generated and is in processing...\n");
                    System.out.println("1 - Go to the menu\n2 - Complete the work with the store");
                    int c2 = in.nextInt();
                    switch (c2) {
                        case 1:
                            break;
                        case 2:
                            System.exit(0);
                            break;
                        default:
                            System.out.println("Incorrect command. Please, try again...");
                            break;
                    }
                }
                break;
                case 6: {
                    if (orders == null) {
                        System.out.println("\nFirst, place your order\n");
                        break;
                    }
                    orders.showAll();
                }
                break;
                case 7: {
                    System.exit(0);
                }
                break;
                default:
                    System.out.println("Incorrect command. Please, try again...");
                    break;
            }
        }

    }
}
